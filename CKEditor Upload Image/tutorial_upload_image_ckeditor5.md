## Tambahan image upload CKEDITOR5 JS

 ✨ By Adityayudhi ✨
 
[![N|Solid](https://sanjayateknologi.com/assets/img/logo/logo-top-nav.png)](https://sanjayateknologi.com)

## File Yang dibutuhkan

- CSS untuk render ke html (Download ckeditor.zip) ambil ckeditor5.css 


## Pada javascript editornya
tambahkan kode berikut
```sh
ckfinder: {
            uploadUrl: "{% url 'url untuk upload file imagenya' %}",
          }
```
Sehingga menjadi seperti dibawah, sisanya disesuaikan saja dengan yang sudah dibuat.
```sh
ClassicEditor.create(document.querySelector('#kt_docs_ckeditor_classic'), {
                    ckfinder: {
                        uploadUrl: "{% url 'url untuk upload file imagenya' %}",
                    }
            })
            .then(editor => {
                frm_isikonten = editor;
                console.log(editor);
            })
            .catch(error => {
                console.error(error);
            });
```

## Pada file py (views)

```sh
import os, uuid
from django.conf import settings
from django.views.decorators.csrf import csrf_exempt

@method_decorator(csrf_exempt, name='dispatch')
class UploadMediaCKEditorViews(View):    
    def post(self, request):
        image = request.FILES.get('upload')
        
        image_name = f'{str(uuid.uuid4())}_{image.name}'

        filename = os.path.join(settings.MEDIA_ROOT, 'ckeditor',  image_name)

        if not os.path.exists(os.path.join(settings.MEDIA_ROOT, 'ckeditor')):
            os.makedirs(os.path.join(settings.MEDIA_ROOT, 'ckeditor'))

        with open(filename, 'wb+') as destination:
            for chunk in image.chunks():
                destination.write(chunk)

        data = {
            "uploaded": 1,
            "fileName": image_name,
            "url": f'{settings.MEDIA_URL}ckeditor/{image_name}'
        }

        return JsonResponse(data)
```

## Pada Rendered HTML File
1. Tambahkan css yang sudah di download

```sh
<link href="{% static 'app/ckeditor5-premium-features/ckeditor5.css' %}" rel="stylesheet">
SESUAIKAN PATHNYA SESUAI KEBUTUHAN MASING-MASING
```
2. Masukkan rendered content ke dalam div dengan class ck-content
```sh
{{ data_berita.content|safe }}
```

