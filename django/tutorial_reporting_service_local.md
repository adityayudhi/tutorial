## Tutorial Reporting Service <In APP>

 ✨ By Adityayudhi ✨
 
[![N|Solid](https://sanjayateknologi.com/assets/img/logo/logo-top-nav.png)](https://sanjayateknologi.com)

## Library Yang dibutuhkan

- playwright==1.48.0 

```sh
---- PASTIKAN TIDAK DALAM ENVIRONTMENT AKTIF ----
## deactivate environtment terlebih dahulu apabila masih berada didalam environtment
pip install playwright==1.48.0 
playwright install
```

## Buat File python baru dengan nama "generate_report_pdf.py"
tambahkan kode berikut
```sh
from playwright.sync_api import sync_playwright
import json, sys, os, glob, time

def generate_report(html_content, options, report_id, html_file_path):
    options = json.loads(option)

    with sync_playwright() as p:
        browser = p.chromium.launch(headless=True, args=[
        "--no-sandbox",
        "--disable-gpu",
        "--disable-extensions",
        "--disable-dev-shm-usage",
        "--disable-software-rasterizer",
        "--no-first-run",
        "--disable-background-networking",
        "--disable-default-apps"
    ])  
        page = browser.new_page()
        page.set_viewport_size({"width": 200, "height": 100})

        if html_content == 'using_file_path':
            page.goto(f"file://{html_file_path}", wait_until="networkidle")
        else:
            page.set_content(html_content, wait_until="networkidle")

        page.pdf(
            **options,
            print_background=True,
        )
        
        browser.close()

    
    pattern = os.path.join(os.getcwd(), 'media', 'temp_files', f"{report_id}*")
    files_to_delete = glob.glob(pattern)
    for file_path in files_to_delete:
        try:
            os.remove(file_path)
            print(f"Deleted: {file_path}")
        except Exception as e:
            print(f"Error deleting {file_path}: {e}")

if __name__ == "__main__":
    html_content = sys.argv[1]
    option = sys.argv[2]
    report_id = sys.argv[3]
    try:
        html_file_path = sys.argv[4]
    except Exception as e:
        html_file_path = ''
    
    generate_report(html_content, option, report_id, html_file_path)
```
## Buat File html yang akan dijadikan report PDF
```sh
{% load static %}
{% load custom_tags %}
<!doctype html>
<html lang="en" dir="ltr">
	<head>
		<title>Invoice</title>
		<!-- <link rel="preload" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap" as="style">
    	<link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto&display=swap"> -->

		<link href="{{url_app}}{% static 'admin/plugins/bootstrap/css/bootstrap.min.css' %}" rel="stylesheet" media='all'/>
		<link href="{{url_app}}{% static 'admin/css/style.min.css' %}" rel="stylesheet"/>
		<link id="theme" rel="stylesheet" type="text/css" media="all" href="{{url_app}}{% static 'admin/colors/color1.min.css' %}" />

		<style type="text/css">
			.invoice_div{
				background-image: url("{{url_app}}{% static 'admin/images/brand/logo.jpg' %}");
			}
			html, body {
			    margin: 0 !important;
			    padding: 0 !important;
			    width: 100% !important;
			    height: 100% !important;
			    font-family: 'Roboto', sans-serif !important;
			}

			.margin0{
				margin-right: 0px !important;
				margin-left: 0px !important;
			}
		</style>
	</head>

	<body class="app sidebar-mini">
		<div class="">
			ISI DENGAN TAMPILAN HTML
		</div>
	</body>
</html>
```

## Pada file py (views)

```sh
data = {
    'data_query': data_query,
    'url_app': f'{request.scheme}://{request.META["HTTP_HOST"]}',
}


pdf_format = {
    'format': 'A4',
    # 'width': "21cm",
    # "height": "21.97cm",
    'margin': {'top':'0cm', 'right':'0cm', 'bottom':'0cm', 'left':'0cm'},
}

pdf_file = generate_report('File_html_yang_akan_dijadikan_report_PDF.html', data, pdf_format)

return HttpResponse(pdf_file, content_type='application/pdf')

# More option https://playwright.dev/python/docs/api/class-page#page-pdf
# display_header_footer=True,
# footer_template="""
#     <div style="width: 100%; text-align: center; font-size: 10px;">
#         Page <span class="pageNumber"></span> of
#         <span class="totalPages"></span>
#         <div class="title"></div>
#         <div class="url"></div>
#         <div class="date"></div>
#     </div>
# """,
```

## Pada file support_function.py

```sh
from django.template.loader import render_to_string
import tempfile, uuid, json, subprocess

def generate_report(html_file, data_html, pdf_options):
    
    report_id = str(uuid.uuid4())

    html_content = render_to_string(html_file, data_html)

    tipe_render = 'using_file_path'

    custom_dir = os.path.join(settings.MEDIA_ROOT, 'temp_files')
    os.makedirs(custom_dir, exist_ok=True)

    with tempfile.NamedTemporaryFile(delete=False, prefix=report_id, suffix='.html', dir=custom_dir, mode="w") as temp_html_file:
            temp_html_file.write(html_content)
            temp_html_file.flush()  # Ensure data is written to disk
            tmp_html_path = temp_html_file.name
            os.chmod(tmp_html_path, 0o770)

    with tempfile.NamedTemporaryFile(delete=False, suffix=".pdf") as tmp_pdf:
        output_path = tmp_pdf.name

    pdf_options['path'] = output_path

    subprocess.run([settings.DEFAULT_PYTHON, os.path.join(settings.BASE_DIR, 'support', "generate_report_pdf.py"), tipe_render, json.dumps(pdf_options), report_id, tmp_html_path])

    file_read = None
    with open(output_path, "rb") as pdf_file:
        file_read =  pdf_file.read()

    os.remove(output_path)
    return file_read
```
## Pada file settings.py
```sh
# Isi sesuai dengan konfigurasi python
DEFAULT_PYTHON = 'python' atau 'python3'
```

