# Cara Konfigurasi Server Production Django

Berikut ini adalah cara konfigurasi aplikasi django pada server ubuntu, disini menggunakan ubuntu versi 22.04.

#### Alat perang :
- Server Production (PUBLIC)
- Putty / CMD (Koneksi SSH ke server)
- Text Editor (Sublime Text, VSCode, Notepad++ , dll)
- WinSCP (Transfer File)

#### Struktur Folder : 
```sh
* /var/www/html/
    * aplikasi/
        * env_aplikasi/
        * nama_project/
            * manage.py dll
            * nama_project/
                * wsgi.py
```

#### Ringkasan Tahapan Installasi : 
- Install NGINX
- Install Postgresql V12 & konfigurasinya
- Install Dependency yang dibutuhkan untuk python - django
- Konfigurasi GUNICORN (Service)
- Konfigurasi Aplikasi Django

## Login Server
Login ke server masing-masing menggunakan SSH dengan port standard 22, menggunakan tools putty atau CMD.
Untuk CMD :
```sh
ssh user@xxx.xxx.xxx.xxx -p 22
```
## Install NGINX
Nginx digunakan sebagai Web Server sebagai interface antara client dengan server
ada beberapa webserver populer diantaranya: apache2, nginx, IIS (Windows). Namun kali ini server menggunakan web server nginx. Berikut ini adalah langkah instalasi NGINX: 

```sh
sudo apt update
sudo apt install nginx
```
Pada tahap ini NGINX sudah berhasil diinstall, selanjutnya pengaturan Firewall

## Pengaturan Firewall
Pengaturan firewall dimaksudkan agar hanya port tertentu saja yang bisa diakses oleh public, ini berkaitan dengan security dari server itu sendiri
`HARUS ALLOW SSH TERLEBIH DAHULU !!!`

```sh
sudo ufw allow ssh
sudo ufw allow 'Nginx Full'
sudo ufw enable
```

## Install Postgresql
Kali ini database yang digunakan adalah postgresql v12, untuk versi yang lain menyesuaikan
```sh
sudo apt update && sudo apt -y install bash-completion wget
```
```sh
curl -fsSL https://www.postgresql.org/media/keys/ACCC4CF8.asc|sudo gpg --dearmor -o /etc/apt/trusted.gpg.d/postgresql.gpg
```
```sh
echo "deb http://apt.postgresql.org/pub/repos/apt/ `lsb_release -cs`-pgdg main" |sudo tee  /etc/apt/sources.list.d/pgdg.list
```
```sh
sudo apt update
```
```sh
sudo apt install postgresql-12 postgresql-client-12
```

Pada tahap ini postgres sudah berhasil terinstall
Selanjutnya adalah konfigurasi postgres, meliputi mengubah sandi user postgres dan pembuatan database untuk aplikasi django.
```sh
sudo su - postgres
```
```sh
psql
```
```sh
ALTER ROLE postgres WITH PASSWORD 'passwordKalian';
```
```sh
CREATE DATABASE nama_database;
```
```sh
\q
```
```sh
logout
```

Sampai pada tahap kali ini seharusnya kalian sudah kembali ke user awal login server.

## Install Dependency Aplikasi Python - Django
```sh
sudo apt install python3-pip python3-virtualenv libpq-dev
```

## Konfigurasi Virtualenv
Buat sebuah folder di dalam [/var/www/html]()
```sh
cd /var/www/html/
sudo mkdir aplikasi
```

#### chown ini perlu untuk memberikan permission kepada folder yang dibuat agar bisa di eksekusi oleh user tersebut

```sh
sudo chown nama_user_server_saat_ini:www-data aplikasi/
cd aplikasi/
virtualenv env_aplikasi
```
#### Aktifkan env_aplikasi
```sh
source env_aplikasi/bin/activate
```

Env yang sudah aktif ditandai dengan `(env_aplikasi)`

#### Copy semua file kedalam folder aplikasi/ sesuai dengan struktur yang dicontohkan diatas (Struktur menyesuaikan dengan aplikasi yang dibuat)

Apabila sudah tercopy silahkan masuk kedalam folder aplikasi dan lakukan perubahan chown
```sh
sudo chown -R nama_user_server_saat_ini:www-data nama_project/
sudo chmod 0770 -R nama_project/
cd nama_project/
```

# Install Package yang Diperlukan Oleh Aplikasi Django
Install package yang diperlukan menggunakan
```sh
pip install nama_package
```

Supaya lebih cepat alangkah baiknya masukkan semua package yang diperlukan kedalam file txt, contoh requirements.txt
berikut contoh requirements.txt
```sh
django==2.2
psycopg2-binary==2.8.6
dll
```
`Selalu sertakan versi pada setiap package !!!`

#### Install package GUNICORN
```sh
pip install gunicorn
```
Untuk GUNICORN optional menyertakan versi

#### Dump Database
Lakukan Dump Database pada lokal machine kalian slenajutnya upload file sql yang sudah ada kedalam server

Lakukan konfigurasi Postgresql agar bisa diakses melalui user selain postgres (user server):
```sh
sudo nano /etc/postgresql/12/main/pg_hba.conf
```
Cari block code yang mengandung 
```sh
# Database administrative login by Unix domain socket
local   all             postgres                                peer
```

Ubah `peer` menjadi `md5` lalu simpan dengan `ctrl + x` dilanjutkan dengan `y` lalu `enter`

Selanjutnya silahkan restart service postgres
```sh
sudo service postgresql restart
```

Next, silahkan lanjutkan dengan me-restore file database kedalam server :

```sh
psql -U postgres -d nama_database < namafilesql.sql
```

#### Konfigurasi settings.py
Silahkan buka file settings.py
```sh
nano nama_project/settings.py
```
dan lakukan beberapa perubahan pada file tersebut: 
```sh
ALLOWED_HOSTS = ['xxx.xxx.xxx.xxx']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'nama_database',
        'USER': 'postgres',
        'PASSWORD': 'password_postgres_kalian',
        'HOST': 'localhost',
        'PORT': port_kalian
    }
}
```
Untuk `PORT` silahkan diisi sesuai port postgres, secara default PORT Standard postgres adalah `5432`
`xxx.xxx.xxx.xxx` adalah IP ADDRESS Server

Setelah melakukan perubahan silahkan simpan file tersebut dengan cara menekan `ctrl+x` lalu `y` lalu `enter`

Pastikan tidak ada error ketika RUN dengan menjalankan server development django
```sh
python manage.py runserver
```

Apabila sukses ditandai dengan adanya output sebagai berikut :
```sh
Starting development server at http://127.0.0.1:8000/
```
Sampai disini langkah konfigurasi django sudah berhasil

## Konfigurasi GUNICORN
Buat service ubuntu untuk django
```sh
sudo nano /etc/systemd/system/nama_project.service
```

Isikan dengan data berikut ini :
```sh
[Unit]
Description=SERVICE DJANGO nama_project
After=network.target

[Service]
User=<userubuntu>
Group=www-data
WorkingDirectory=/var/www/html/aplikasi/nama_project
ExecStart=/var/www/html/aplikasi/env_aplikasi/bin/gunicorn --access-logfile - --workers 3 --bind unix:/var/www/html/aplikasi/nama_project.sock <nama_project>.wsgi:application

[Install]
WantedBy=multi-user.target
```
Silahkan ganti : 
- `<userubuntu>` sesuai user ubuntu sama ketika login ssh server
- `<nama_project>` sesuai dengan folder dimana file `wsgi.py` berada
- Untuk nama_project yang ada di `Description` silahkan diisi sesuai dengan deskripsi masing-masing, tidak harus sama dengan nama_project

Setelah melakukan perubahan silahkan simpan file tersebut dengan cara menekan `ctrl+x` lalu `y` lalu `enter`

Selanjutnya start service yang sudah dibuat dengan : 
```sh
sudo service nama_project start
sudo service nama_project status
```
Apabila service berhasil dijalankan, akan ada log status `active (running)` seperti berikut :
```sh
nama_project.service - SERVICE DJANGO nama_project
    Loaded: loaded (/etc/systemd/system/nama_project.service; disabled; vendor preset: enabled)
    Active: active (running) since Sat 2024-03-02 19:12:56 WIB; 1min 51s ago
```

serta terdapat file nama_project.sock didalam folder [/var/www/html/aplikasi/]()

Setelah sukses silahkan buat service yang tadi berjalan otomatis saat system reboot
```sh
sudo systemctl enable nama_project
```

Sampai disini, konfigurasi gunicorn sudah selesai

## Konfigurasi Vhost NGINX
Konfigurasi ini untuk akses aplikasi dari web browser. Jadi apabila diakses menggunakan IP xxx.xxx.xxx.xxx akan menampilkan halaman yang sudah dibuat
```sh
sudo nano /etc/nginx/sites-available/nama_project.conf
```
Isikan dengan data berikut :
```sh
server {
        listen 80;
        server_name xxx.xxx.xxx.xxx;

        root /var/www/html/aplikasi/nama_project;

         location / {
                include proxy_params;
                proxy_pass http://unix:/var/www/html/aplikasi/nama_project.sock;
            }

        location /static/ { alias /var/www/html/aplikasi/nama_project/assets_root/; }
}
```

Ubah `xxx.xxx.xxx.xxx` dengan IP Server kalian
Untuk Block `location /static/ { alias /var/www/html/aplikasi/nama_project/assets_root/; }` silahkan isi berdasarkan lokasi folder static (css, js, font dll)

Selanjutnya lakukan perintah berikut untuk membuat shorcut kedalam folder `sites-enabled/` nginx agar dapat dieksekusi oleh nginx
```sh
sudo ln -s /etc/nginx/sites-available/nama_project.conf /etc/nginx/sites-enabled/
```
Hapus `default` config dari nginx
```sh
sudo rm /etc/nginx/sites-enabled/default
```

Silahkan reload nginx
```sh
sudo service nginx reload
```

Sampai Disini Seharusnya instalasi Django pada Server Ubuntu telah sukses dan selesai. 
Silahkan akses IP Address server dari web browser, apabila sudah sukses maka akan menampilkan halaman aplikasi yang telah dibuat.

# TIPS KONFIGURASI SERVER UBUNTU
- Selalu gunakan huruf kecil pada penamaan folder ataupun file
- Hindari penggunakan spasi sebagai pemisah nama folder atau file
- Selalu Double Check Spelling dalam penulisan 
- Apabila ada info terkait `permission denied` saat eksekusi perintah diatas, silahkan coba dengan menggunakan `sudo` didepan perintah. Ex: sudo service nginx reload
- Apabila ada error silahkan dicermati kesalahan ada dimana, 
- Cek error / status nginx : `sudo service nginx status`
- Cek Error / status postgres : `sudo service postgresql status`
- Cek Error / Service : `sudo service nama_service status`
- Apabila ada error 502 Gateway 404 Dll silahkan cek pada file error nginx default pada /var/log/nginx/error.log -> `sudo nano /var/log/nginx/errror.log`

